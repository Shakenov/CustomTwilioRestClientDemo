﻿namespace CustomTwilioRestClientDemo.Models
{
    public class MessageModel
    {
        public string To { get; set; }
        public string From { get; set; }
        public string Message { get; set; }
    }
}